#!/usr/bin/env bash
set -e
docker-compose -p stud-face-local -f compose-local.yml down
docker-compose -p stud-face-local -f compose-local.yml up -d