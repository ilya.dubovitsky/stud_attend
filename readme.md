## Get Python

You need Python 3.7 to run this software.

## Download weights
Download weights for RESNET:

https://1drv.ms/u/s!AhMqVPD44cDOhkPsOU2S_HFpY9dC

rename to ***model_cpu_final.pth***
and move to the folder ***cv/work_space/save/***

Download weights for MTCNN:

https://drive.google.com/file/d/1KmkH1AewjgO_1dMsPDyhTJoHtHANFQty/view?usp=sharing

unzip it and save to any folder you want

but you to specify the full path to this folder in the ***cv/mtcnn/detect.py*** file, line 10, ***MTCNN_DIR*** variable

## Install required libraries

You can either use **pipenv**, which is highly recommended, or **pip**.
 
For pipenv run:

``
pipenv install
``

For pip run:

``
pip install -R requirements.txt
``

## Install docker

The latest version of docker is preferred.

## Create database

For this you only need to run:

``
sudo chmod +x ./start-local-postgres.sh
``

``
./start-local-postgres.sh
``

You can delete the container after you have tested the software.

## Run commands to setup Django

``
python manage.py migrate
``

``
python manage.py createsuperuser
``

During the creation of the user, set the login and password to use in the web interface.

## Run the server

``
python manage.py runserver
``

## Go and check

Visit http://localhost:8000/admin/.

You can:

1. Create a student
2. Add Recognition
3. After the recognition is done, check the recognized images and attendances.