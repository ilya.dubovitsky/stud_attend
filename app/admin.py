from django.contrib import admin
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe
from django.urls import path
from django.http import HttpResponse, HttpResponseRedirect

from django.core.files.uploadedfile import InMemoryUploadedFile

import cv2

from .models import *


class InlineImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, **kwargs):
        output = []

        if value and getattr(value, "url", None):
            image_url = value.url
            file_name = str(value)
            output.append(
                f'<a href="{image_url}" target="_blank">'
                f'<img src="{image_url}" alt="{file_name}" width="150" height="150" style="object-fit: cover;"/>'
                f'</a>')

        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class AttendanceInline(admin.TabularInline):
    model = Attendance
    extra = 0

    formfield_overrides = {models.ImageField: {'widget': InlineImageWidget}}


@admin.register(Recognition)
class RecognitionAdmin(admin.ModelAdmin):
    change_list_template = 'recognition_changelist.html'

    list_display = ('pk', 'timestamp', 'recognized',)

    inlines = (AttendanceInline,)

    readonly_fields = ('photo_thumbnail',)
    
    def photo_thumbnail(self, obj):
        return mark_safe(f'<img src="{obj.photo.url}" width="200"/>')

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('recognize/', self.recognize),
        ]
        return my_urls + urls

    def recognize(self, request):
        video_capture = cv2.VideoCapture(0)
        # Check success
        if not video_capture.isOpened():
            raise Exception("Could not open video device")
        # Read picture. ret === True on success
        ret, frame = video_capture.read()
        # Close device
        video_capture.release()
        # Write to file
        cv2.imwrite('image.png', frame)
        
        img_crop_pil = Image.fromarray(frame[:, :, ::-1])
        byte_io = BytesIO()
        img_crop_pil.save(byte_io, format='JPEG')
        image = InMemoryUploadedFile(byte_io, None, 'foo.jpg', 'image/jpeg',
                                  byte_io.__sizeof__(), None)
        img_crop_pil.close()
        recognition = Recognition.objects.create(photo=image)
    
        return HttpResponseRedirect('../')

@admin.register(Attendance)
class AttendanceAdmin(admin.ModelAdmin):
    list_display = ('student', 'recognition', 'distance', 'similarity',)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'frequency',)

    readonly_fields = ('photo_thumbnail',)

    def photo_thumbnail(self, obj):
        return mark_safe(f'<img src="{obj.photo.url}" width="200"/>')
