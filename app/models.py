from io import BytesIO

from PIL import Image
from django.contrib.postgres.fields import ArrayField
from django.core.files.base import ContentFile
from django.db import models

from core.settings import img_embeddings
from utils import match_people


class Recognition(models.Model):
    photo = models.ImageField(upload_to='photos')

    timestamp = models.DateTimeField(auto_now_add=True)

    recognized = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.full_clean()
        models.Model.save(self, force_insert, force_update, using, update_fields)

        if not self.recognized and self.photo:
            # Find embeddings
            embeddings, faces, bboxes, landmarks = img_embeddings(self.photo.path)

            # Find matches
            matches = match_people(embeddings, True)

            # Remove previous
            prev = Attendance.objects.filter(recognition=self).all()
            for prev_att in prev:
                prev_att.delete()

            # Create new matches
            for i, match in enumerate(matches):
                if match is not None:
                    attendant = Attendance.objects.create(student=match[0],
                                                          embedding=list(embeddings[i]),
                                                          distance=match[1],
                                                          similarity=match[1],
                                                          recognition=self)

                    f = BytesIO()
                    b, g, r = faces[i].split()
                    face = Image.merge("RGB", (r, g, b))
                    face.save(f, format='jpeg')
                    s = f.getvalue()
                    attendant.photo.save('recognized-face', ContentFile(s))
                    f.close()
                    # attendant.photo.save('recognized.jpg', InMemoryUploadedFile(
                    #     faces[i], None, 'recognized.jpg', 'image/jpeg', faces[i].tell, None))
                    attendant.save()

            self.recognized = True

        models.Model.save(self, force_insert=False, force_update=True, using=using, update_fields=update_fields)

    def __str__(self):
        return f'Recognition at {self.timestamp}'


class Student(models.Model):
    name = models.CharField(max_length=32)

    photo = models.ImageField(upload_to='photos')

    centroid = ArrayField(models.FloatField(), size=512, null=True, blank=True)
    initial_embedding = ArrayField(models.FloatField(), size=512, null=True, blank=True)
    residual_embedding = ArrayField(models.FloatField(), size=512, null=True, blank=True)

    frequency = models.PositiveIntegerField(default=0, null=False, blank=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.full_clean()
        models.Model.save(self, force_insert, force_update, using, update_fields)

        if not len(self.initial_embedding) and self.photo:
            embedding = list(img_embeddings(self.photo.path)[0][0])
            self.initial_embedding = embedding
            self.residual_embedding = [0] * 512
            self.centroid = embedding

        models.Model.save(self, force_insert=False, force_update=True, using=using, update_fields=update_fields)

    def __str__(self):
        return f'{self.name}'


class Attendance(models.Model):
    bbox = ArrayField(ArrayField(models.FloatField(), size=2), size=4, null=True, blank=True)
    landmarks = ArrayField(ArrayField(models.FloatField(), size=2), size=5, null=True, blank=True)

    photo = models.ImageField(upload_to='photos', blank=True, null=True)

    embedding = ArrayField(models.FloatField(), size=512)

    student = models.ForeignKey(Student, models.CASCADE, null=True, blank=True)

    recognition = models.ForeignKey(Recognition, models.CASCADE, null=True, blank=True)

    distance = models.FloatField(null=True, blank=True)
    similarity = models.FloatField(null=True, blank=True)
