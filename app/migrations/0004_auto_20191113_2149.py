# Generated by Django 3.0b1 on 2019-11-13 21:49

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20191029_1045'),
    ]

    operations = [
        migrations.AddField(
            model_name='attendance',
            name='distance',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='recognition',
            name='recognized',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='attendance',
            name='bbox',
            field=django.contrib.postgres.fields.ArrayField(base_field=django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), size=2), blank=True, null=True, size=4),
        ),
        migrations.AlterField(
            model_name='attendance',
            name='landmarks',
            field=django.contrib.postgres.fields.ArrayField(base_field=django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), size=2), blank=True, null=True, size=5),
        ),
    ]
