from typing import List

import numpy as np


def time_weighted_average(old_embedding: List, new_embedding: List, n: int) -> List:
    """ Calculate weighted average of all student's
    photos embeddings except the initial one.

    Arguments:
        old_embedding {np.ndarray} -- average of all previous embeddings
        new_embedding {np.ndarray} -- embedding of the new student's photo
        n {int} -- number of student's old embeddings (not including the initial and the new embeddings)

    Returns:
        np.ndarray -- averaged embedding of all photos (except the initial one)
    """
    return list((n * np.array(old_embedding) + 2 * np.array(new_embedding)) / (n + 2))


def total_average(initial_embedding: List, time_weighted_embedding: List, alpha: float = 0.7) -> List:
    """Calculate the weighted average of all embeddings
    according to the formula from the report.

    Arguments:
        initial_embedding {np.ndarray} -- embedding of the initial perfect photo
        time_weighted_embedding {np.ndarray} -- average of all subsequent embeddings

    Keyword Arguments:
        alpha {float} -- weighting proportion coefficient  (default: {0.7})

    Returns:
        np.ndarray -- resulting averaged embedding
    """
    return list(alpha * np.array(initial_embedding) + (1 - alpha) * np.array(time_weighted_embedding))


def update_student(student, new_embedding: List):
    """
    Update embeddings and centroid of a student according to our proposed method.

    :param student: to be updated
    :param new_embedding: an embedding ou new photo
    """
    student.residual_embedding = time_weighted_average(student.residual_embedding, new_embedding, student.frequency)
    student.centroid = total_average(student.initial_embedding, student.residual_embedding)
    student.frequency += 1

    student.save()
