import numpy as np
import os
import hnswlib

from django.apps import apps
from django.conf import settings

from weighted_average import update_student


def get_index():
    # Reiniting, loading the index
    index = hnswlib.Index(space='cosine', dim=512)
    if os.path.isfile(settings.INDEX_PATH):
        index.load_index(settings.INDEX_PATH, max_elements=settings.MAX_ELEMENTS)
    else:
        print('Creating new index...')
        students = apps.get_model('app', 'Student').objects.all()
        stud_embeddings = np.zeros((len(students), 512), dtype=np.float)
        stud_labels = np.zeros((len(students)), dtype=np.int)
        for i, stud in enumerate(students):
            stud_embeddings[i] = np.array(stud.centroid)
            stud_labels[i] = stud.id
        index.init_index(max_elements=settings.MAX_ELEMENTS, ef_construction=100, M=16)
        index.add_items(stud_embeddings, stud_labels)
        return index


def match_people(embeddings, with_nones=False):
    """
    Find matches for each face on the photo.

    :param embeddings: all embedding from the photo
    :param with_nones: fill with nones not matched.
    :return: list of matched student objects and distances with similarities
    """
    # students = apps.get_model('app', 'Student').objects.all()
    # matched = []
    #
    #
    # for emb in embeddings:
    #     match = None
    #
    #
    #     for stud in students:
    #         emb = np.array(emb)
    #         stud_emb = np.array(stud.centroid)
    #
    #         dist = np.linalg.norm(emb - stud_emb)
    #         sim = np.dot(emb, stud_emb.T) / np.linalg.norm(emb) / np.linalg.norm(stud_emb)
    #         print(f'student {stud}; distance = {dist}; similarity = {sim}')
    #         if dist < settings.DIST_THRESH and sim > settings.SYM_THRESH:
    #             match = (stud, dist, sim)
    #             update_student(stud, emb)
    #             break
    #
    #     if with_nones or matched is not None:
    #         matched.append(match)
    index = get_index()
    labels, distances = index.knn_query(embeddings, k=1)

    students = list(map(lambda x: apps.get_model('app', 'Student').objects.get(id=x[0]), labels))
    matches = list(zip(students, distances))
    index.save_index(settings.INDEX_PATH)
    return matches
